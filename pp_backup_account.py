#!/usr/bin/env python
"""
Created by Omer ben shushan
App name: pyPanel Cli
Description: backup account
"""

import sys,os
import zipfile

def zip_file_name(account_name):
	import time

	creation_date = time.strftime("%d_%m_%Y")
	return creation_date + "_" + account_name + ".zip"

def zipdir(path, ziph):
    # ziph is zipfile handle
    for root, dirs, files in os.walk(path):
        for file in files:
            ziph.write(os.path.join(root, file))

def main(options):
	account_name = options.account_name
	zip_name = zip_file_name(account_name)

	zipf = zipfile.ZipFile(zip_name, 'w')
	zipdir('/home/' + account_name +'/', zipf)
	zipf.close()

if __name__ == "__main__":
	from optparse import OptionParser

	usage = "Usage: %prog <params>"
	parser = OptionParser(usage=usage)
	parser.add_option("--account", dest="account_name",
		help="the account name for backup")

	(options, args) = parser.parse_args()
	if options.account_name is None:
		print("Error: you must specify account name to backup")
		parser.print_help()
		sys.exit(1)

	main(options)
